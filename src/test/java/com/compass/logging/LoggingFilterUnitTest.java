package com.compass.logging;

import java.io.ByteArrayOutputStream;
import java.util.logging.Handler;
import java.util.logging.Logger;
import java.util.logging.StreamHandler;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

public class LoggingFilterUnitTest {

  private final Logger logger = Logger.getLogger(DefaultLogHandler.class.getName());
  private final ByteArrayOutputStream tempOut = new ByteArrayOutputStream();

  private StreamHandler streamHandler;

  @BeforeEach
  public void setUpStreams() {
    Handler[] handlers = logger.getParent().getHandlers();
    streamHandler = new StreamHandler(tempOut, handlers[0].getFormatter());

    logger.addHandler(streamHandler);
  }

  @Test
  public void testFilter() throws Exception {
    // Given (with setup)
    MockHttpServletRequest request = new MockHttpServletRequest();
    MockHttpServletResponse response = new MockHttpServletResponse();
    String requestBody = "{\"req\" : 23}";

    request.setRequestURI("/path?query=q1");
    request.setContent(requestBody.getBytes());
    request.setContentType("application/json");
    request.setMethod("POST");

    // When
    new LoggingFilter().doFilterInternal(request, response, new MockChain());

    // Then
    streamHandler.flush(); // Reveal logs
    String output = tempOut.toString();

    assert output.contains(request.getRequestURL().toString());
    assert output.contains(requestBody); //request
    assert output.contains(response.getContentAsString()); //response
    assert output.contains(Integer.toString(response.getStatus()));
  }
}
