package com.compass.logging;

import com.compass.logging.model.LogModel;
import jakarta.servlet.FilterChain;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.logging.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.ContentCachingResponseWrapper;

@Component
public class LoggingFilter extends OncePerRequestFilter {

  private final LogHandler handler;

  public LoggingFilter() {
    this.handler = new DefaultLogHandler();
  }

  @Autowired
  public LoggingFilter(LogHandler handler) {
    this.handler = handler;
  }

  @Override
  protected void doFilterInternal(
      HttpServletRequest request,
      HttpServletResponse response,
      FilterChain filterChain
  ) throws IOException {
    // We should use wrapper for request too, because input stream closes after request
    ContentCachingRequestWrapper requestWrapper = new ContentCachingRequestWrapper(request);
    ContentCachingResponseWrapper responseWrapper = new ContentCachingResponseWrapper(response);

    Level logLevel = Level.INFO;
    String responseBody = "";

    try {
      filterChain.doFilter(requestWrapper, responseWrapper);
    } catch (Exception ex) {
      logLevel = Level.SEVERE;
      responseBody = String.format("Threw exception: %s", ex.getMessage());
    }

    // Get body stream from response and copy to output again because stream is emptied
    if (responseBody.isEmpty()) {
      responseBody = new String(responseWrapper.getContentAsByteArray());

      responseWrapper.copyBodyToResponse();
    }

    handler.handle(
        this.createLogModel(
            new String(requestWrapper.getContentAsByteArray()),
            responseBody,
            this.createURLWithQuery(requestWrapper),
            logLevel,
            responseWrapper.getStatus()
        )
    );
  }

  private String createURLWithQuery(ContentCachingRequestWrapper request) {
    String url = request.getRequestURL().toString(); // URL path included except queries
    String query = request.getQueryString();

    if (query != null) {
      url = String.format("%s?%s", url, query);
    }

    return url;
  }

  private LogModel createLogModel(
      String requestBody, String responseBody, String url, Level logLevel, int statusCode
  ) {
    LogModel logModel = new LogModel();

    logModel.setLevel(logLevel);
    logModel.setUrl(url);
    logModel.setRequestBody(requestBody);
    logModel.setResponseBody(responseBody);
    logModel.setStatus(HttpStatus.valueOf(statusCode));

    return logModel;
  }
}
