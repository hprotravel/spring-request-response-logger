package com.compass.logging;

import com.compass.logging.model.LogModel;
import org.springframework.stereotype.Component;

@Component
public interface LogHandler {
	void handle(LogModel model);
}
