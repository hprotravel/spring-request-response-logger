package com.compass.logging;

import com.compass.logging.model.LogModel;
import java.util.logging.Logger;

public class DefaultLogHandler implements LogHandler {

  Logger logger = Logger.getLogger(DefaultLogHandler.class.getName());

  private static final String REPLACEMENT_REGEX = "\\s{2,}|\\n";

  @Override
  public void handle(LogModel model) {
    logger.log(
      model.getLevel(),
      "URL: {0} - Request body: {1} - Response body: {2} - Status: {3}",
      new String[]{
        model.getUrl(),
        model.getRequestBody().replaceAll(REPLACEMENT_REGEX, ""),
        model.getResponseBody().replaceAll(REPLACEMENT_REGEX, ""),
        model.getStatus().toString()
      }
    );
  }
}
