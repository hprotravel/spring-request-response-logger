## 0.5.0 (September 06, 2024)
  - Set Java version for Jitpack and remove Apache Commons

## 0.4.0 (September 06, 2024)
  - Upgrade Java to 17 and Spring Boot to 3.0

## 0.3.0 (August 24, 2022)
  - Feature - CK-214 - Do not call FilterChain again

## 0.2.0 (July 01, 2022)
  - Improvement - CK-82 - Refactor package name
  - Improvement - CK-103 - Remove mvnw.cmd and improve README

## 0.1.0 (January 18, 2022)
  - Bugfix - CK-22 - Extract some functions into methods
  - Bugfix - CK-22 - Handle exception logs and use Java logging library instead of System.out
  - Added unit test and some fixes
  - Removed mvn files and refactored pom.xml
  - Initial project commit
  - Initial commit

