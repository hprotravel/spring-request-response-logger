# Spring Boot Request & Response Logger

Spring Boot package for logging any incoming requests and outgoing responses

### Summary

It has a default logging handler which is logging URL, request, response and status code to console. You can also implement your own Handler interface instead of using default.

### Usage

You should add JitPack repository for importing dependency:

```
<repositories>
    <repository>
        <id>jitpack.io</id>
        <url>https://jitpack.io</url>
    </repository>
</repositories>
```

Then you can just add Maven dependency like this:

```
<dependencies>
    ..............
    <!-- request response logger !-->
    <dependency>
	    <groupId>org.bitbucket.hprotravel</groupId>
    	<artifactId>spring-request-response-logger</artifactId>
    </dependency>
    ..............
</dependencies>
```

And define bean in configuration as:

```
@Bean
public LoggingFilter loggingFilter() {
    return new LoggingFilter();
}
```

When any request sent to a URL, it automatically logs request and response to console by default log handler. If you want to implement your own log handler instead of default, you should define bean like this:

```
@Bean
public LoggingFilter loggingFilter() {
    return new LoggingFilter(new LogHandlerImpl());
}
```

